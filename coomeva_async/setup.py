from setuptools import setup

setup(name='coomeva_async',
      description='Coomeva async client manager',
      url='https://bitbucket.org/quantil/coomeva_async',
      author='Simon Ramirez Amaya',
      author_email='simon.ramirez@quantil.com.co',
      license='MIT',
      packages=['coomeva_async'],
      install_requires=['requests','grequests'],
      zip_safe=False)
