import grequests
import requests
import random



def obtain_clients(number_of_clients):
    """
    Returns a list of clients represented as dictionaries
    """
    batch_size = 400
    all_clients = []
    unique_clients = 0
    current_clients = {}

    batch_counter = 0
    while unique_clients < number_of_clients:
        # print("batch # " + str(batch_counter + 1))
        left = number_of_clients - unique_clients
        local_batch_size = min(left, batch_size)
        responses = obtain_batch(local_batch_size)
        result = add_responses(responses, current_clients)
        all_clients.extend(result["unique_clients"])
        unique_clients += result["number_unique_clients"]
        batch_counter = batch_counter + 1

    return all_clients


def add_responses(responses, current_clients):
    """
    Iterates through the responses and returns
    a dict with 3 values:
    1. unique_clients: a list of unique clients
    2. number_unique_clients: number of unique clientes on the list
    3. wrong_responses: # of failed responses

    """
    unique_clients = []
    number_unique_clients = 0
    wrong_responses = 0
    for response in responses:
        if response is None:
            print("None response")
            wrong_responses = wrong_responses + 1
        else:
            status_code = str(response.status_code)
            if status_code.startswith("2"):
                client = response.json()[0]
                nk_cliente = client["nk_cliente"]
                if nk_cliente not in current_clients:
                    number_unique_clients = number_unique_clients + 1
                    current_clients[nk_cliente] = 1
                    unique_clients.append(client)
            else:
                wrong_responses = wrong_responses + 1
    
    return {"unique_clients": unique_clients,
     "number_unique_clients": number_unique_clients,
     "wrong_responses": wrong_responses
     }


def obtain_batch(number_of_clients):
    protocolo = 'http://'
    servidor = ['192.168.0.4:8080/','192.168.0.5:8080/','192.168.0.6:8080/', '192.168.0.7:8080/','192.168.0.8:8080/', '192.168.0.9:8080/']
    balanceador = ['192.168.0.4:8080/', '192.168.0.16:8080/']
    servidor_2 = ['192.168.0.4:8080/']
    endpoint = 'consulta/consultar-clientes/'
    urls = [protocolo + random.choice(balanceador) + endpoint for i in range(0,number_of_clients)]
    headers = {'Content-Type': 'application/json'}
    session = requests.Session()
    rs = (grequests.get(url,headers=headers,session=session,timeout=60) for url in urls)
    responses = grequests.map(rs)
    # process_responses(responses)
    return responses

def obtain_clients_from_list(nk_clients, want_dict=False):
    """
    Returns an dict with all the clients from the list.
    """
    clients_dict = {}
    missing_clients = 0
    missing_clients_list = []
    status = "OK"
    length = len(nk_clients)
    batch_size = 600
    n_regular_batches = int(length / batch_size)
    last_batch_size = length - n_regular_batches * batch_size
    start_points_of_batches = []
    sizes_of_batches = []
    i = 0
    while i < n_regular_batches:
        start_points_of_batches.append(i * batch_size)
        sizes_of_batches.append(batch_size)
        i = i + 1

    if last_batch_size > 0:
        start_points_of_batches.append(i * batch_size)
        sizes_of_batches.append(last_batch_size)

    for index, item in enumerate(start_points_of_batches):
        # print("############################")
        # print("COMIENZA batch # %i" % (index + 1))
        # print("############################")
        start_point = item
        current_batch_size = sizes_of_batches[index]
        batch_clients = nk_clients[start_point:start_point + current_batch_size]
        result = obtain_clients_data(batch_clients)
        missing_clients = missing_clients + result["missing_clients"]
        missing_clients_list.extend(result["missing_nk_clientes_list"])
        batch_clients_dict = result["client_dict"]
        if result["status"] == "FAILED":
            status = result["status"]
        for nk_cliente, client in batch_clients_dict.items():
            if client is not None:
                clients_dict[nk_cliente] = client

    # this response contains both the dict with None values and a list without them
    response = {
        "clients_without_none" : list(filter(None.__ne__,list(clients_dict.values()))),
        "missing_clients" : missing_clients, 
        "status": status,
        "missing_clients_list": missing_clients_list
    }
    if want_dict:
        response["clients_dict"] = clients_dict
    return response

def obtain_clients_data(batch_nk_clients):
    """
    Returns a list with clients requested in the batch
    """

    local_client_dict = {}
    all_nk_clientes_responses_received = False
    
    protocolo = 'http://'
    servidor = ['192.168.0.4:8080/','192.168.0.5:8080/','192.168.0.6:8080/', '192.168.0.7:8080/','192.168.0.8:8080/', '192.168.0.9:8080/']
    balanceador = ['192.168.0.4:8080/', '192.168.0.16:8080/']
    endpoint = 'consulta/consultar-cliente/'
    
    tries = 0
    keys_to_repeat = batch_nk_clients
    while not all_nk_clientes_responses_received and tries < 5:
        urls = []
        for nk_client in keys_to_repeat:
            local_client_dict[nk_client] = None # for now, this space will be filled later
            url = protocolo + random.choice(balanceador) + endpoint + nk_client
            urls.append(url)

        session = requests.Session()
        rs = (grequests.get(url,session=session,timeout=60) for url in urls)
        responses = grequests.map(rs)
        extract_clients_result = extract_clients(responses, local_client_dict)
        # keys to repeat queda solo las que tienen None como value
        keys_to_repeat = none_in_dict(local_client_dict)["none_keys"]
        # sacar de keys to repeat las que ya se que son 404
        keys_to_repeat = [x for x in keys_to_repeat if x not in extract_clients_result["not_found_nk_clients"]]
        if len(keys_to_repeat) == 0:
            all_nk_clientes_responses_received = True
        #results = missing_clients_report(local_client_dict, extract_clients_result)
        tries = tries + 1

    if all_nk_clientes_responses_received:
        result = {
            "client_dict" : local_client_dict,
            "status": "OK",
            "missing_clients": 0,
            "missing_nk_clientes_list": keys_to_repeat
        }
    else:
        result = {
            "client_dict" : local_client_dict,
            "status": "FAILED",
            "missing_clients": none_in_dict(local_client_dict)["none_counter"],
            "missing_nk_clientes_list": keys_to_repeat
        }
    return result


def extract_clients(responses, local_client_dict):
    """
    Process the responses for the nk_clients. Saves them in local_client_dict
    """
    not_found_nk_clients = []
    none_responses = 0
    not_found_responses = 0
    server_error_responses = 0
    unkwown_error_responses = 0
    for response in responses:
        if response is None:
            print("None response")
            none_responses = none_responses + 1
        else:
            status_code = str(response.status_code)
            if status_code.startswith("2"):
                client = response.json()
                nk_cliente = client["nk_cliente"]
                local_client_dict[nk_cliente] = client
            elif status_code == "404":
                nk_cliente = response.text.split(":")[0]
                not_found_nk_clients.append(nk_cliente)
                not_found_responses = not_found_responses + 1
            elif status_code.startswith("5"):
                server_error_responses = server_error_responses + 1
            else:
                unkwown_error_responses = unkwown_error_responses + 1
    
    total_missing_responses = none_responses + unkwown_error_responses + not_found_responses + server_error_responses
    return {
        "not_found_nk_clients": not_found_nk_clients,
        "unkwown_error_responses": unkwown_error_responses,
        "none_responses": none_responses,
        "not_found_responses": not_found_responses,
        "server_error_responses": server_error_responses,
        "total_missing_responses": total_missing_responses

    }
    

def missing_clients_report(local_client_dict, extract_clients_result):
    
    none_results = none_in_dict(local_client_dict)
    report = {
        "none_responses_counter": none_results["none_counter"],
        "none_nk_cliente_list": none_results["none_keys"],
        "not_found_nk_clients": extract_clients_result["not_found_nk_clients"],
        "unkwown_error_responses_counter": extract_clients_result["unkwown_error_responses"],
        "not_found_responses_counter": extract_clients_result["not_found_responses"],
        "server_error_responses_counter": extract_clients_result["server_error_responses"],
        "total_error_responses_counter": extract_clients_result["total_error_responses"]
    }
    return report


def none_in_dict(d):
    """
    counts the number of None values in a dictionary
    returns the count and the keys with None Values
    """
    none_counter = 0
    none_keys = []
    for key, value in d.items():
        if value is None:
            none_counter =  none_counter + 1
            none_keys.append(key)

    return {"none_counter": none_counter, "none_keys": none_keys}

def generate_report(responses):
    """
    Process the response and prints a report with the results
    """
    ok = 0
    not_ok = 0
    counter_1xx = 0
    counter_2xx = 0
    counter_3xx = 0
    counter_4xx = 0
    counter_5xx = 0
    for response in responses:
        if response is None:
            not_ok = not_ok + 1
        else:
            status_code = str(response.status_code)
            if status_code.startswith("2"):
                counter_2xx = counter_2xx + 1
                ok = ok + 1
            elif status_code.startswith("1"):
                counter_1xx = counter_1xx + 1
                not_ok = not_ok + 1
            elif status_code.startswith("3"):
                counter_3xx = counter_3xx + 1
                not_ok = not_ok + 1
            elif status_code.startswith("4"):
                counter_4xx = counter_4xx + 1
                not_ok = not_ok + 1
            elif status_code.startswith("5"):
                counter_5xx = counter_5xx + 1
                not_ok = not_ok + 1

    print(str("# ok responses: " + str(ok)))
    print(str("# failed responses: " + str(not_ok)))
    print("----------------- SPECIFIC RESULTS ----------------")
    print(str("# of 1xx responses: " + str(counter_1xx)))
    print(str("# of 2xx responses: " + str(counter_2xx)))
    print(str("# of 3xx responses: " + str(counter_3xx)))
    print(str("# of 4xx responses: " + str(counter_4xx)))
    print(str("# of 5xx responses: " + str(counter_5xx)))
    print("---------------------------------------------------")


