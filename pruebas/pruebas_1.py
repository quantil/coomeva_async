from coomeva_async import client_manager
from timeit import default_timer as timer
import simplejson

sizes_one = [1000]
sizes_small = [10, 50, 100, 500, 1000, 5000]
sizes_large = [100, 500, 1000, 10000, 25000]
size_10000 = [10000]
size_50000 = [50000]


def write_json(items, name):
    """
    Writes a JSON file with the contents of the list
    """
    list_as_string = simplejson.dumps(items, ignore_nan=True, default=str)
    file_name = name + '.json'
    with open(file_name, 'w') as f:
        f.write(list_as_string)

for size in sizes_small:
    print("--------- SIZE: " + str(size))
    t_start = timer()
    clients = client_manager.obtain_clients(size)
    if size == 10:
        write_json(clients, "muestra")
    t_end = timer()
    print("number of clients: " + str(len(clients)))

    total_time = t_end - t_start
    print("timpo total timer: "+ str(total_time))


