from coomeva_async import client_manager
from timeit import default_timer as timer
import simplejson
import os

cwd = os.getcwd()
path_to_nkcliente_files = cwd + "/clientes/"

name = "clientes.txt"
sizes_one = [3]
small_sizes = [3, 10, 50, 100, 1000]
large_sizes = [10000, 25000]

def write_json(items, name):
    """
    Writes a JSON file with the contents of the list
    """
    list_as_string = simplejson.dumps(items, ignore_nan=True, default=str)
    file_name = name + '.json'
    with open(file_name, 'w') as f:
        f.write(list_as_string)

def verify_nk_clientes(nk_clientes, clients_dict):
    error = 0
    for nk_cliente in nk_clientes:
        if nk_cliente not in clients_dict:
            error = error + 1
        
    ok = len(nk_clientes) - error
    return {"ok": ok , "errors": error}


for size in small_sizes:
    print("------------ TEST for -------------")
    print("--------- SIZE: " + str(size) + "--------------")
    t_start = timer()
    filename = str(size) + "_" + name
    full_path = path_to_nkcliente_files + filename
    nk_clientes = [line.rstrip('\n') for line in open(full_path)]
    clients_result = client_manager.obtain_clients_from_list(nk_clientes)
    #results = verify_nk_clientes(nk_clientes, clients_result["clients"])
    print("-------- RESULTS ----------")
    print("status: " + clients_result["status"])
    print("length of client list: " + str(len(clients_result["clients_without_none"])))
    print("missing clients counter: " + str(clients_result["missing_clients"]))
    print("length missing clients list: " + str(len(clients_result["missing_clients_list"])))
    print("-------- RESULTS ----------")

    if size < 50:
        write_json(clients_result, "muestra" + str(size))
    t_end = timer()

    total_time = t_end - t_start
    print("timpo total timer: "+ str(total_time))
    print("-----------@@@@@-----------------------")
    print("-----------@@@@@-----------------------")
    print("-----------@@@@@-----------------------")

